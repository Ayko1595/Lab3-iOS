//
//  LocationManager.swift
//  RunMateApp
//
//  Created by Konstantin Ay on 2017-11-08.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import CoreLocation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() { }
}
