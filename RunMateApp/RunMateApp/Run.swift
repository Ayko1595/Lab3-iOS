//
//  Run.swift
//  RunMateApp
//
//  Created by Konstantin Ay on 2017-11-08.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import Foundation

struct Run {
    var distance: Double
    var duration: Int
    var timestamp: Date
}
