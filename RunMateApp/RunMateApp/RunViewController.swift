//
//  RunViewController.swift
//  RunMateApp
//
//  Created by Konstantin Ay on 2017-11-02.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import AVKit

class RunViewController: UIViewController {

    var run: Run!
    
    private let locationManager = LocationManager.shared
    private var toggleDisplaySeconds = 0
    private var seconds = 0
    private var timer: Timer?
    private var applauseDistance = Measurement(value: 0, unit: UnitLength.kilometers)

    private var distance = Measurement(value: 0, unit: UnitLength.kilometers)
    private var locationList: [CLLocation]! = []
    
    final let ZERO_TIME: Date = Date()
    final let ZERO_DISTANCE: Double = 0.0
    
    var isShowing: Bool = true
    
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let applauseFile = Bundle.main.path(forResource: "applause", ofType: "wav")
        
        do{
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: applauseFile!))
        }catch{
            print(error)
        }
        
        detailView.layer.cornerRadius = 20
        timeLabel.text = "00:00"
        distanceLabel.text = String(0.0)
        paceLabel.text = "00:00"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(toggleDetails))
        detailView.addGestureRecognizer(tap)
        
        startRun()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func toggleDetails(){
        if isShowing {
            UIView.animate(withDuration: 2, animations: {
                self.detailView.alpha = 0.2
                self.isShowing = false
            })
        }else{
            UIView.animate(withDuration: 2, animations: {
                self.detailView.alpha = 1
                self.isShowing = true
            })
        }
        
        toggleDisplaySeconds = 0
    }
    
    private func startRun() {
        toggleDisplaySeconds = 0
        seconds = 0
        distance = Measurement(value: 0, unit: UnitLength.kilometers)
        applauseDistance = Measurement(value: 0, unit: UnitLength.kilometers)
        locationList.removeAll()
        updateDisplay()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.eachSecond()
        }
        startLocationUpdates()
        mapView.removeOverlays(mapView.overlays)
    }
    
    private func stopRun() {
        locationManager.stopUpdatingLocation()
    }
    
    private func startLocationUpdates() {
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
    }
    
    func eachSecond() {
        toggleDisplaySeconds += 1
        seconds += 1
        updateDisplay()
    }
    
    private func updateDisplay() {
        
        let formattedDistance = distance.value / 1000
        
        if (applauseDistance.value / 1000) >= 1.0 {
            applauseDistance = Measurement(value: 0, unit: UnitLength.kilometers)
            audioPlayer.play()
        }
        
        let formattedTime = FormatDisplay.time(seconds)
        let formattedPace = FormatDisplay.pace(distance: distance,
                                               seconds: seconds,
                                               outputUnit: UnitSpeed.minutesPerKilometer)
        
        distanceLabel.text = String(format:"%.2f", formattedDistance)
        timeLabel.text = "\(formattedTime)"
        paceLabel.text = "\(formattedPace)"
        
        if  isShowing && (toggleDisplaySeconds % 10) == 0 {
            toggleDisplaySeconds = 0
            toggleDetails()
        }
    }
    
    private func polyLine() -> MKPolyline {
        guard let locations = locationList else {
            return MKPolyline()
        }
        
        let coords: [CLLocationCoordinate2D] = locations.map { location in
            let location = location
            return CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        }
        return MKPolyline(coordinates: coords, count: coords.count)
    }

}

extension RunViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = UIColor(red: 255/255, green: 189/255, blue: 52/255, alpha: 1)
        renderer.lineWidth = 3
        return renderer
    }
}

extension RunViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.kilometers)
                applauseDistance = applauseDistance + Measurement(value: delta, unit: UnitLength.kilometers)
                let coordinates = [lastLocation.coordinate, newLocation.coordinate]
                mapView.add(MKPolyline(coordinates: coordinates, count: 2))
                let region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 100, 100)
                mapView.setRegion(region, animated: true)
            }
            
            locationList.append(newLocation)
        }
    }
}
